---
title: Link
info: Links used for linkin pages etc.
---
When linking happens in a text, especially in game design, the user should not be too distracted as he or she is mainly there to play.

Therefore we decided to make every link text only bold. When hovering over it, the user get’s a slide animation of a line coming under the text to confirm that he is truly on a link.

{% include pattern_block.html url='/src/patterns/components/link/link.html' %}
