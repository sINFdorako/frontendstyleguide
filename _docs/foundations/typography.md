---
title: Typography
info: Pricedown font has been selected as the primary font for this system.
---

When thinking of GTA or a similar game the user expects something special or in other words something recognisable. Here we decided to go with a classical GTA-Style font where users immediately see that this is a game and not some boring website. 

The most common font associated with the series is "Pricedown" which has been used on all titles since Grand Theft Auto III.



{% include pattern_block.html url='/src/patterns/foundations/typography/pricedown.html' %}

---

## Fallback

A `sans-serif` fallback font is used incase [@font-face](https://www.w3schools.com/cssref/css3_pr_font-face_rule.asp) is not supported in the target browser. 

---

## Copryright 

Roboto and Roboto Slab are free software. For more details, please visit Google Fonts official website at [https://fonts.google.com/](https://fonts.google.com/)
