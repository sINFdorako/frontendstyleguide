---
title: Color
info: We have decided to use bright colours in this style guide.
---
The reason for bright colours is that the game we are designing this structure for should encourage the user to have fun with it and play with it.

Therefore, choosing the right colours is very important.

{% include pattern_block.html url='/src/patterns/foundations/color/primary-colors.html' %}

{% include pattern_block.html url='/src/patterns/foundations/color/accent-colors.html' %}
