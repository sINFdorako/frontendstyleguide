---
title: Input
info: Inputs are used to enter data in freetext format. The input includes support for all HTML5 types. 
---


{% include pattern_block.html url='/src/patterns/components/input/default.html' %}

Default input field.

{% include pattern_block.html url='/src/patterns/components/input/disabled.html' %}

Use disabled input field, e.g. if the user is not allowed to enter data.

{% include pattern_block.html url='/src/patterns/components/input/wrong.html' %}

Use required input fields, e.g. for password fields.
