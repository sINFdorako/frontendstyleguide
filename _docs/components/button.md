---
title: Button
info: Buttons are clickable items used to perform an action.
nav: true
---

# Basic Buttons

A button can contain text. Although any tag can be used for a button, it will only be keyboard focusable if you use a `button` tag or you add the property `tabindex="0"`.

Buttons should be used in situations where users might need to:

- submit a form
- begin a new task
- trigger a new UI element to appear on the page
- specify a new or next step in a process

{% include pattern_block.html url='/src/patterns/components/button/default_button.html' %}

{% include pattern_block.html url='/src/patterns/components/button/disabled_button.html' %}

A button can be disabled, pointer events will not occur.
