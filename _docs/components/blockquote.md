---
title: Blockquote
info: Blockquotes are used to emphasise a portion of the body content.
---
Here we decided to go with a simple design for the blockquote. The purpose is that the text should stand clearly out but again not too much as it would distract the user. And as already said the gaming should be in focus here. The block quote in particular helps the user to recognise important information very quickly.
{% include pattern_block.html url='/src/patterns/components/blockquote/blockquote.html' %}
