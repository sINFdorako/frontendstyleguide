---
title: Card
info: Cards are used to apply a container around a related grouping of information.
nav: true
---

# Cards

A card can contain text and pictures. To initialize a card, apply the card class to an <div> element.


{% include pattern_block.html url='/src/patterns/components/card/card.html' %}

