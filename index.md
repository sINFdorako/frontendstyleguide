---
title: Overview
layout: doc
info: Welcome to our GTA Salzburg styleguide.
nav: false
---

<h1>👋 Hi! </h1>

This is a design styleguide for our game web page "GTA Salzburg". It is a game based on Grand Theft Auto. Out style guide provide a colourful experience.


## Code Convention

- For CSS Normalize we use normalize.css by Nicolas Gallagher.
- For formatting, we use the Prettier package included in the IDEs.
- CSS classes are written according to BEM guidelines as far as possible.
- The css architecture is based on the Simple SCSS Playbook.



# License

This tool is free for everyone to use, and modify, but don't try and sell it.
Available under [Apache license version 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).

It was built using [Jekyll](https://jekyll.rb), it's [design principle]({{ site.baseurl }}/docs/about/02-design-principle.html) is based on the [US Design Standards](https://designsystem.digital.gov/design-principles/).

[View License File](https://github.com/matthewelsom/jekyll-style-guide/blob/master/LICENSE)
